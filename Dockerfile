FROM alpine:latest

ENV VERSION=latest

COPY *.sh ./

RUN echo "http://mirror.math.princeton.edu/pub/alpinelinux/edge/testing" >> /etc/apk/repositories && \
  apk update                        && \
  apk add openjdk16-jre curl sed jq
  
CMD ["sh", "dockercmd.sh"]