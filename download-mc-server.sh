if [ "$VERSION" == "latest" ]
then 
    SELECTEDVERSION=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | jq -r '.latest.release')
else
    SELECTEDVERSION=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | jq -r --arg BANANA "$VERSION" '.versions[] | select(.id==$BANANA) | .id')
fi
echo "Selected Minecraft version = $SELECTEDVERSION"
SELECTEDLINK=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | jq -r --arg SELECTED "$SELECTEDVERSION" '.versions[] | select(.id==$SELECTED) | .url')
DOWNLOADLINK=$(curl -s $SELECTEDLINK | jq -r  .downloads.server.url)
echo "Downloading server.jar for $SELECTEDVERSION"
curl $DOWNLOADLINK -O