echo "Accepting License Agreement..."
java -Xms1024M -Xmx1024M -jar server.jar nogui > /dev/null 2>&1
sed -i 's/eula=false/eula=true/g' eula.txt
echo "Done! Starting server now :)"
java -Xms1024M -Xmx4096M -jar server.jar nogui